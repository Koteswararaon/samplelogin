import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http:HttpClient) { }
  public sharedCustomer = [];
  adminData(){
    return this.http.get('../assets/data.json');
  }
  getRegistrationData(){
    return this.sharedCustomer;
  }
}
