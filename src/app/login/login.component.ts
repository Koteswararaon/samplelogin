import { AdminService } from './../admin.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
   login:any = {};
   isValid:boolean = false;
  constructor(private logincheck:AdminService, private router: Router) { }

  ngOnInit(): void {
  }
  onSubmit(){
    let usersData = this.logincheck.sharedCustomer;

    if(usersData.length > 0){
      usersData.forEach((d)=>{
        if(d.email == this.login.username && d.password == this.login.password ){
          
          this.router.navigate(['/','admin']);
        }else{
          
          this.isValid = true;
        }
      })
    } else{
      alert('Please register....');
      this.router.navigate(['/','signup']);
    }
    //alert(JSON.stringify(this.login));
  }

}
