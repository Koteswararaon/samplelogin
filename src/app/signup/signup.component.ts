import { Router } from '@angular/router';
import { AdminService } from './../admin.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  model: any = {};

  
  constructor(private serviceCall:AdminService,private router:Router) { }

  ngOnInit(): void {
    
  }
  onSubmit() {
    this.serviceCall.sharedCustomer.push(this.model);
    console.log(this.serviceCall.sharedCustomer);
    this.model = {};
    this.router.navigate(['/','login']);
  }

}
