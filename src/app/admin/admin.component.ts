import { AdminService } from './../admin.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  data;
  constructor(private _http:AdminService) { }

  ngOnInit(): void {
    this.data=this._http.getRegistrationData();
    console.log('admin data'+this.data);

  }

}
